//
// kernel.cpp
//
// Circle - A C++ bare metal environment for Raspberry Pi
// Copyright (C) 2017-2019  R. Stange <rsta2@o2online.de>
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
#include "kernel.h"
#include <circle/memio.h>
#include <circle/usb/usbkeyboard.h>
#include <circle/string.h>
#include <circle/util.h>
	
#define GPIO_FIQ_PIN		27		
	
#define TRACE_DEPTH		20			// number of trace entries

typedef struct GPIO_bits_ 
{
	unsigned char DATA  			: 8;            
	unsigned int  ADDR  			: 8;
	unsigned int  ADDR_HI  			: 7;
	
	unsigned char pwm 				: 1; //GPIO 23              
	unsigned char ack 				: 1; //GPIO 24
    unsigned char mem 				: 1; //GPIO 25
    unsigned char rw  				: 1; //GPIO 26
    unsigned char fiqTrigger 		: 1; //GPIO 27
	
    unsigned char GP28 				: 1;
    unsigned char GP29 				: 1;
    unsigned char GP30 				: 1;
    unsigned char GP31 				: 1;
		
} GPIO_bits;

typedef union 
{
	u32 value;
	GPIO_bits b;
} packed_GPIO;


CKernel *CKernel::s_pThis = 0;

packed_GPIO pGPIO;

static const char FromKernel[] = "kernel";

CKernel::CKernel (void)
:	m_Screen (m_Options.GetWidth (), m_Options.GetHeight ()),
	m_Timer (&m_Interrupt),
	m_Logger (m_Options.GetLogLevel (), &m_Timer),
	m_USBHCI (&m_Interrupt, &m_Timer),

	m_InputPinFIQ (GPIO_FIQ_PIN, GPIOModeInput, &m_Interrupt),
	m_Tracer (TRACE_DEPTH, FALSE)
{
	s_pThis = this;
	
}

CKernel::~CKernel (void)
{
	s_pThis = 0;
}

boolean CKernel::Initialize (void)
{
	boolean bOK = TRUE;

	if (bOK)
	{
		bOK = m_Screen.Initialize ();
	}

	if (bOK)
	{
		bOK = m_Serial.Initialize (115200);
	}

	if (bOK)
	{
		CDevice *pTarget = m_DeviceNameService.GetDevice (m_Options.GetLogDevice (), FALSE);
		if (pTarget == 0)
		{
			pTarget = &m_Screen;
		}

		bOK = m_Logger.Initialize (pTarget);
	}

	if (bOK)
	{
		bOK = m_Interrupt.Initialize ();
	}

	if (bOK)
	{
		bOK = m_Timer.Initialize ();
	}

	if (bOK)
	{
		bOK = m_USBHCI.Initialize ();
	}
	
	return bOK;
}

u32 previous_value;

TShutdownMode CKernel::Run (void)
{
	m_Logger.Write (FromKernel, LogNotice, "Compile time: " __DATE__ " " __TIME__);

	
	
	
	CUSBKeyboardDevice *pKeyboard = (CUSBKeyboardDevice *) m_DeviceNameService.GetDevice ("ukbd1", FALSE);
	
	if (pKeyboard == 0)
	{
		m_Logger.Write (FromKernel, LogError, "Keyboard not found");

		return ShutdownHalt;
	}
	
	pKeyboard->RegisterKeyStatusHandlerRaw (KeyStatusHandlerRaw);
//	pKeyboard->RegisterKeyPressedHandler (KeyPressedHandler);
	
	write32( ARM_GPIO_GPFSEL0, 0 ); // GPIO 0 to 9 in
    write32( ARM_GPIO_GPFSEL1, 0 ); // GPIO 10 to 19 in
	write32( ARM_GPIO_GPFSEL1+4, 0x1000 ); // GPIO 20 to 29 in, except 24 out
	
	//m_Tracer.Start ();

	m_InputPinFIQ.ConnectInterrupt (FIQHandler, this);
	m_InputPinFIQ.EnableInterrupt2 (GPIOInterruptOnFallingEdge);

	//now we just wait
	while (1)
	{
		//if (pGPIO.value != previous_value)
//		{
//			m_Logger.Write (FromKernel, LogNotice, "pGPIO.value = %d", pGPIO.value);
//			previous_value = pGPIO.value;
//		}
	}

	m_InputPinFIQ.DisableInterrupt2 ();

	//m_Tracer.Dump ();

	return ShutdownHalt;
}

unsigned char outbyte, div2byte;
unsigned char keyboard_row;
unsigned char keys[16] = { 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff };

unsigned char bitwise (unsigned char var, unsigned char bit, unsigned char value)
{
	if (value)
	
		return var |= 1UL << bit;
	
	else
		
		return var &= 1UL << bit;

}

void CKernel::FIQHandler (void *pParam)
{
	pGPIO.value = read32 (ARM_GPIO_GPLEV0);

	if (pGPIO.b.rw != 0) //read
	{

		write32( ARM_GPIO_GPCLR0, 0x01000000); // hold the /WAIT	
			
		write32( ARM_GPIO_GPFSEL0, 0x00249249 ); // GPIO 0 to 7 out, 8 and 9 in
	
		switch(pGPIO.b.ADDR)
		{
			
			//Divide by 2 interface
			case 0xC0:
				outbyte = div2byte;
				break;
			
			//read keyboard
			case 0xA9: 
				//printf("leitura A9 %d\n",keyboard_row&0x0f);
				outbyte = keys[(keyboard_row&0x0f)];
				break;
				
			default:
				outbyte = 0b11111111;
				break;
		}
		
		//send the "outbyte" to the bus
		write32( ARM_GPIO_GPCLR0, 0x000000ff); //all bits as '0'	
		write32( ARM_GPIO_GPSET0, 0x00000000 + outbyte); //set the '1' bits 
		
		write32( ARM_GPIO_GPSET0, 0x01000000); // Pulse the GPIO 24 	
		write32( ARM_GPIO_GPCLR0, 0x01000000); // to release the /WAIT	
	
		pGPIO.value = read32 (ARM_GPIO_GPLEV0);	
		


	}
	else //write
	{		
		write32( ARM_GPIO_GPFSEL0, 0 ); // GPIO 0 to 9 in
		
		//	pGPIO.value = ptr_GPLEV0[ 0 ];  //precisa ler novamente aqui???
		
		switch( pGPIO.b.ADDR )
		{
			
			//Divide by 2 interface
			case 0xC0:
			
				div2byte = pGPIO.b.DATA >> 1;
				break;
			
			//write keyboard row
			case 0xAA:
				keyboard_row = pGPIO.b.DATA;

				
				break;
				
			case 0xAB:
				if ((pGPIO.b.DATA>>7) & 1)
				{
					switch((pGPIO.b.DATA>>1) & 7)
					{
							case 0: keyboard_row = bitwise (keyboard_row, 0, (pGPIO.b.DATA & 1)); break;
							case 1: keyboard_row = bitwise (keyboard_row, 1, (pGPIO.b.DATA & 1)); break;
							case 2: keyboard_row = bitwise (keyboard_row, 2, (pGPIO.b.DATA & 1)); break;
							case 3: keyboard_row = bitwise (keyboard_row, 3, (pGPIO.b.DATA & 1)); break;
					}
				}
				break;
		
		}
		
	}
	
}

void CKernel::KeyPressedHandler (const char *pString)
{
	assert (s_pThis != 0);
	s_pThis->m_Screen.Write (pString, strlen (pString));
}


void CKernel::KeyStatusHandlerRaw (unsigned char ucModifiers, const unsigned char RawKeys[6])
{
	assert (s_pThis != 0);

	CString Message;
	Message.Format ("Key status (modifiers %02X)", (unsigned) ucModifiers);

	//clear the key buffer
	for (unsigned i = 0; i < 16; i++)	
		keys[i] = 0xff;
	
	//apply the modifiers
	if (((ucModifiers >> 5) & 1) || ((ucModifiers >> 1) & 1)) keys[6] &= 0xFE;  // Shift
	if (((ucModifiers >> 4) & 1) || ((ucModifiers >> 0) & 1)) keys[6] &= 0xFD;  // Ctrl 
	if ((ucModifiers >> 2) & 1) keys[6] &= 0xFB;  // Graph 
	if ((ucModifiers >> 6) & 1) keys[6] &= 0xEF;  // Code 
				
	for (unsigned i = 0; i < 6; i++)
	{
		if (RawKeys[i] != 0)
		{
			CString KeyCode;
			KeyCode.Format (" %02X", (unsigned) RawKeys[i]);

			Message.Append (KeyCode);
			
			switch(RawKeys[i])
			{
				case 0x27: keys[0] &= 0xFE; break; // 0 
				case 0x1e: keys[0] &= 0xFD; break; // 1 
				case 0x1f: keys[0] &= 0xFB; break; // 2 
				case 0x20: keys[0] &= 0xF7; break; // 3 
				case 0x21: keys[0] &= 0xEF; break; // 4 
				case 0x22: keys[0] &= 0xDF; break; // 5 
				case 0x23: keys[0] &= 0xBF; break; // 6 
				case 0x24: keys[0] &= 0x7F; break; // 7 
				
				case 0x25: keys[1] &= 0xFE; break; // 8 
				case 0x26: keys[1] &= 0xFD; break; // 9 
				case 0x2d: keys[1] &= 0xFB; break; // - 
				case 0x2e: keys[1] &= 0xF7; break; // + 
				case 0x31: keys[1] &= 0xEF; break; // | 
				case 0x2f: keys[1] &= 0xDF; break; // [ 
				case 0x30: keys[1] &= 0xBF; break; // ] 
				case 0x33: keys[1] &= 0x7F; break; // ; 
				
				case 0x34: keys[2] &= 0xFE; break; // ' 
				case 0x32: keys[2] &= 0xFD; break; // ` 
				case 0x36: keys[2] &= 0xFB; break; // , 
				case 0x37: keys[2] &= 0xF7; break; // . 
				case 0x38: keys[2] &= 0xEF; break; // / 
//				case 0x2f: keys[2] &= 0xDF; break; // [ 
				case 0x04: keys[2] &= 0xBF; break; // A 
				case 0x05: keys[2] &= 0x7F; break; // B 
				
				case 0x06: keys[3] &= 0xFE; break; // C
				case 0x07: keys[3] &= 0xFD; break; // D 
				case 0x08: keys[3] &= 0xFB; break; // E 
				case 0x09: keys[3] &= 0xF7; break; // F 
				case 0x0A: keys[3] &= 0xEF; break; // G 
				case 0x0B: keys[3] &= 0xDF; break; // H 
				case 0x0C: keys[3] &= 0xBF; break; // I 
				case 0x0D: keys[3] &= 0x7F; break; // J 
				
				case 0x0E: keys[4] &= 0xFE; break; // K
				case 0x0f: keys[4] &= 0xFD; break; // L 
				case 0x10: keys[4] &= 0xFB; break; // M 
				case 0x11: keys[4] &= 0xF7; break; // N 
				case 0x12: keys[4] &= 0xEF; break; // O 
				case 0x13: keys[4] &= 0xDF; break; // P 
				case 0x14: keys[4] &= 0xBF; break; // Q 
				case 0x15: keys[4] &= 0x7F; break; // R 

				case 0x16: keys[5] &= 0xFE; break; // S
				case 0x17: keys[5] &= 0xFD; break; // T 
				case 0x18: keys[5] &= 0xFB; break; // U 
				case 0x19: keys[5] &= 0xF7; break; // V 
				case 0x1A: keys[5] &= 0xEF; break; // W 
				case 0x1B: keys[5] &= 0xDF; break; // X 
				case 0x1C: keys[5] &= 0xBF; break; // Y 
				case 0x1D: keys[5] &= 0x7F; break; // Z 
				
				case 0xe1: 						   // Left Shift
				case 0xe5: keys[6] &= 0xFE; break; // Right Shift
				case 0xe0: 						   // Left Ctrl 
				case 0xe4: keys[6] &= 0xFD; break; // Right Ctrl 
				case 0xe2: keys[6] &= 0xFB; break; // Graph 
				case 0x39: keys[6] &= 0xF7; break; // CAPS 
				case 0xe6: keys[6] &= 0xEF; break; // Code 
				case 0x3a: keys[6] &= 0xDF; break; // F1 
				case 0x3b: keys[6] &= 0xBF; break; // F2 
				case 0x3c: keys[6] &= 0x7F; break; // F3
				
				case 0x3D: keys[7] &= 0xFE; break; // F4
				case 0x3E: keys[7] &= 0xFD; break; // F5 
				case 0x29: keys[7] &= 0xFB; break; // ESC 
				case 0x2B: keys[7] &= 0xF7; break; // TAB 
				case 0x4B: keys[7] &= 0xEF; break; // STOP (pg up)
				case 0x2A: keys[7] &= 0xDF; break; // BS 
				case 0x4D: keys[7] &= 0xBF; break; // SELECT (end)
				case 0x28:						   // RETURN 
				case 0x58: keys[7] &= 0x7F; break; // RETURN 				
				
				case 0x2c: keys[8] &= 0xFE; break; // SPACE
				case 0x4a: keys[8] &= 0xFD; break; // HOME 
				case 0x49: keys[8] &= 0xFB; break; // INS 
				case 0x4C: keys[8] &= 0xF7; break; // DEL 
				case 0x50: keys[8] &= 0xEF; break; // LEFT 
				case 0x52: keys[8] &= 0xDF; break; // UP 
				case 0x51: keys[8] &= 0xBF; break; // DOWN
				case 0x4F: keys[8] &= 0x7F; break; // RIGHT 

				case 0x55: keys[9] &= 0xFE; break; // NUM *
				case 0x57: keys[9] &= 0xFD; break; // NUM +
				case 0x54: keys[9] &= 0xFB; break; // NUM /
				case 0x62: keys[9] &= 0xF7; break; // NUM 0
				case 0x59: keys[9] &= 0xEF; break; // NUM 1
				case 0x5A: keys[9] &= 0xDF; break; // NUM 2
				case 0x5B: keys[9] &= 0xBF; break; // NUM 3
				case 0x5C: keys[9] &= 0x7F; break; // NUM 4

				case 0x5D: keys[10] &= 0xFE; break; // NUM 5
				case 0x5E: keys[10] &= 0xFD; break; // NUM 6
				case 0x5F: keys[10] &= 0xFB; break; // NUM 7
				case 0x60: keys[10] &= 0xF7; break; // NUM 8
				case 0x61: keys[10] &= 0xEF; break; // NUM 9
				case 0x56: keys[10] &= 0xDF; break; // NUM -
			//	case 0x51: keys[10] &= 0xBF; break; // NUM ,
				case 0x63: keys[10] &= 0x7F; break; // NUM .

				}
		}
	}
	
	s_pThis->m_Logger.Write (FromKernel, LogNotice, Message);
}
