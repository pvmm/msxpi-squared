//
// Pi Squared
// Copyright (c) 2019 - Victor Trucco
//
// All rights reserved
//
// Redistribution and use in source and synthezised forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// Redistributions in synthesized form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution.
//
// Neither the name of the author nor the names of other contributors may
// be used to endorse or promote products derived from this software without
// specific prior written permission.
//
// THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// You are responsible for any legal issues arising from your use of this code.
//
//-------------------------------------------------------------------------------

#define ARMBASE 0x8000
#define CS 0x20003000
#define CLO 0x20003004
#define C0 0x2000300C
#define C1 0x20003010
#define C2 0x20003014
#define C3 0x20003018

// Timer ----------------

#define ARM_TIMER_LOD 0x2000B400
#define ARM_TIMER_VAL 0x2000B404
#define ARM_TIMER_CTL 0x2000B408
#define ARM_TIMER_CLI 0x2000B40C
#define ARM_TIMER_RIS 0x2000B410
#define ARM_TIMER_MIS 0x2000B414
#define ARM_TIMER_RLD 0x2000B418
#define ARM_TIMER_DIV 0x2000B41C
#define ARM_TIMER_CNT 0x2000B420

// GPIO -----------------

#define GPFSEL0 0x20200000 // GPIO 0 a 9
#define GPFSEL1 0x20200004 // GPIO 10 a 19
#define GPFSEL2 0x20200008 // GPIO 20 a 29
#define GPFSEL3 0x2020000C // GPIO 30 a 39
#define GPSET0  0x2020001C // set bits
#define GPCLR0  0x20200028 // clear bits
#define GPLEV0  0x20200034 // GPIO 0...31     , GPLEV1 has the values for pins 32->53.
#define GPEDS0 	0x20200040 // GPIO Pin Event Detect Status 0 
#define GPFEN0 	0x20200058 // GPIO 0..31 Pin Falling Edge Detect Enable 0 (setar 1 no bit para detectar)
#define GPAFEN0 0x20200088 // GPIO 0..31 Pin Async. Falling Edge Detect 0 (setar 1 no bit para detectar)
#define GPLEN0 	0x20200070 // GPIO 0..31 Pin LOW Detect Enable 0 (setar 1 no bit para detectar)
#define GPPUD		0x20200094 //GPIO Pin Pull-up/down Enable 
#define GPPUDCLK0 	0x20200098 //GPIO Pin Pull-up/down Enable Clock 0 

// IRQ -------------- 

#define IRQ_PENDING2    0x2000B208
#define IRQ_ENABLE2     0x2000B214
#define IRQ_FIQ_CTRL    0x2000B20C
#define IRQ_ENABLE_BASIC 0x2000B218


unsigned int *ptr_GPEDS0 = ( unsigned int * )GPEDS0;
unsigned int *ptr_GPLEV0 = ( unsigned int * )GPLEV0;
 
extern void PUT32 ( unsigned int, unsigned int );
extern void PUT16 ( unsigned int, unsigned int );
extern void PUT8 ( unsigned int, unsigned int );
extern unsigned int GET32 ( unsigned int );
extern unsigned int GETPC ( void );
extern void BRANCHTO ( unsigned int );
extern void dummy ( unsigned int );

extern void enable_irq ( void );
extern void enable_fiq ( void );

#define SCREEN_WIDTH 320
#define SCREEN_HEIGHT 240
#define BITS_PER_PIXEL 8

#define BORDER_TOP  24
#define BORDER_LEFT  32


typedef struct video_bits_ 
{
    unsigned char x 		: 5;	 
	unsigned char y2 		: 3;
	unsigned char pixel_row : 3;
	unsigned char y1 		: 2;
	unsigned char holder 	: 3;
} video_bits;

typedef union 
{
	unsigned int value;
	video_bits b;
} packed_video;

typedef struct attr_ 
{
    unsigned char ink 		: 3;	 
	unsigned char paper 	: 3;
	unsigned char bright 	: 1;
	unsigned char flash 	: 1;
} attr;

typedef union 
{
	unsigned char value;
	attr b;
} packed_attr;

typedef struct GPIO_bits_ 
{
	unsigned char DATA  			: 8;            
	unsigned int  ADDR  			: 8;
	unsigned int  ADDR_HI  			: 7;
	
	unsigned char pwm 				: 1; //GPIO 23              
	unsigned char ack 				: 1; //GPIO 24
    unsigned char mem 				: 1; //GPIO 25
    unsigned char rw  				: 1; //GPIO 26
    unsigned char fiqTrigger 		: 1; //GPIO 27
	
    unsigned char GP28 				: 1;
    unsigned char GP29 				: 1;
    unsigned char GP30 				: 1;
    unsigned char GP31 				: 1;
		
} GPIO_bits;

typedef union 
{
	unsigned long value;
	GPIO_bits b;
} packed_GPIO;




packed_GPIO pGPIO;


unsigned char buffer_video[SCREEN_WIDTH * SCREEN_HEIGHT * 2];




void memcpy ( unsigned char *d, unsigned char *s, unsigned len )
{
	while(--len)   *d++ = *s++;
}

unsigned int MailboxWrite ( unsigned int fbinfo_addr, unsigned int channel )
{
    unsigned int mailbox;

    mailbox = 0x2000B880;
	
    while( 1 )
    {
        if(( GET32( mailbox + 0x18 ) & 0x80000000 ) == 0 ) break;
    }
	
    PUT32( mailbox + 0x20, fbinfo_addr + channel );
	
    return( 0 );
}

unsigned int MailboxRead ( unsigned int channel )
{
    unsigned int ra;
    unsigned int mailbox;

    mailbox = 0x2000B880;
	
    while( 1 )
    {
        while( 1 )
        {
            ra = GET32( mailbox + 0x18 );
            if (( ra & 0x40000000 ) ==0 ) break;
        }
       
        ra = GET32( mailbox + 0x00 );
        
        if (( ra&0xF ) == channel ) break;
    }
	
    return(ra);
}

unsigned int frameBuffer;

void put_pixel ( int x, int y, int c )
{

    // calculate the pixel's byte offset inside the buffer
    unsigned int pix_offset = x + y * SCREEN_WIDTH;
	
	PUT8( frameBuffer + pix_offset, c ); 

}

 unsigned int fb_pos;




void initVideo ( void )
{

	//-------------- configura Video
	
	
	PUT32(0x40040000, 0x000000c0);	//   // FB_STRUCT_END - FB_STRUCT ; Buffer Size In Bytes (Including The Header Values, The End Tag And Padding)
	PUT32(0x40040004, 0x00000000); 	// ; Buffer Request/Response Code
									//; Request Codes: $00000000 Process Request Response Codes: $80000000 Request Successful, $80000001 Partial Response
									//; Sequence Of Concatenated Tags
									
	PUT32(0x40040008, 0x00048003);	//Set_Physical_Display ; Tag Identifier
	PUT32(0x4004000c, 0x00000008); 	//Value Buffer Size In Bytes
	PUT32(0x40040010, 0x00000008);	// 1 bit (MSB) Request/Response Indicator (0=Request, 1=Response), 31 bits (LSB) Value Length In Bytes
	PUT32(0x40040014, SCREEN_WIDTH);//; Value Buffer
	PUT32(0x40040018, SCREEN_HEIGHT);// ; Value Buffer

	PUT32(0x4004001c, 0x00048004);	  //Set_Virtual_Buffer ; Tag Identifier
	PUT32(0x40040020, 0x00000008);    // ; Value Buffer Size In Bytes
	PUT32(0x40040024, 0x00000008);    //; 1 bit (MSB) Request/Response Indicator (0=Request, 1=Response), 31 bits (LSB) Value Length In Bytes
	PUT32(0x40040028, SCREEN_WIDTH);  //; Value Buffer
	PUT32(0x4004002c, SCREEN_HEIGHT); // ; Value Buffer

	PUT32(0x40040030, 0x00048005);	//Set_Depth ; Tag Identifier
	PUT32(0x40040034, 0x00000004);  //Value Buffer Size In Bytes
	PUT32(0x40040038, 0x00000004);  //1 bit (MSB) Request/Response Indicator (0=Request, 1=Response), 31 bits (LSB) Value Length In Bytes
	PUT32(0x4004003c, BITS_PER_PIXEL); //Value Buffer

	PUT32(0x40040040, 0x00048009);	//Set_Virtual_Offset ; Tag Identifier
	PUT32(0x40040044, 0x00000008); 	//Value Buffer Size In Bytes
	PUT32(0x40040048, 0x00000008); 	//1 bit (MSB) Request/Response Indicator (0=Request, 1=Response), 31 bits (LSB) Value Length In Bytes

	PUT32(0x4004004c, 0);			// FB_OFFSET_X:
	PUT32(0x40040050, 0);			// FB_OFFSET_Y:

	PUT32(0x40040054, 0x0004800b);	//Set_Palette ; Tag Identifier
	PUT32(0x40040058, 0x00000048);  //Value Buffer Size In Bytes
	PUT32(0x4004005c, 0x00000048);  //1 bit (MSB) Request/Response Indicator (0=Request, 1=Response), 31 bits (LSB) Value Length In Bytes
	PUT32(0x40040060, 0); 			//Value Buffer (Offset: First Palette Index To Set (0-255))
	PUT32(0x40040064, 16);			// Value Buffer (Length: Number Of Palette Entries To Set (1-256))

	
	PUT32(0x40040068, 0xFF000000);  //  BLACK       	
	PUT32(0x4004006c, 0xFFA00000);  //  BLUE    
	PUT32(0x40040070, 0xFF0000A0);  //  RED        		
	PUT32(0x40040074, 0xFFA000A0);  //  MAGENTA   
	PUT32(0x40040078, 0xFF00A000);	//  GREEN         
	PUT32(0x4004007c, 0xFFA0A000);	//  CYAN       		
	PUT32(0x40040080, 0xFF00A0A0);	//  YELLOW       
	PUT32(0x40040084, 0xFFA0A0A0);	//  WHITE    
                      
	PUT32(0x40040088, 0xFF000000);  //  BRIGHT_BLACK  
	PUT32(0x4004008c, 0xFFFF0000);  //  BRIGHT_BLUE   
	PUT32(0x40040090, 0xFF0000FF);	//  BRIGHT_RED  	
	PUT32(0x40040094, 0xFFFF00FF);	//  BRIGHT_MAGENTA
	PUT32(0x40040098, 0xFF00FF00);	//  BRIGHT_GREEN  
	PUT32(0x4004009c, 0xFFFFFF00);	//  BRIGHT_CYAN 	
	PUT32(0x400400a0, 0xFF00FFFF);	//  BRIGHT_YELLOW 
	PUT32(0x400400a4, 0xFFFFFFFF); 	//  BRIGHT_WHITE  
	
	PUT32(0x400400a8, 0x00040001);	//Allocate_Buffer ; Tag Identifier
	PUT32(0x400400ac, 0x00000008);  //Value Buffer Size In Bytes
	PUT32(0x400400b0, 0x00000008);  //1 bit (MSB) Request/Response Indicator (0=Request, 1=Response), 31 bits (LSB) Value Length In Bytes

	PUT32(0x400400b4, 0) ; 			//Pointer para o Framebuffer
	PUT32(0x400400b8, 0) ; 			

	PUT32(0x400400bc, 0) ; 			//(End Tag)

	MailboxWrite(0x40040000,8);
	MailboxRead(8);
	
	frameBuffer = GET32(0x400400b4);
	

}



int c=1;
int d=1;
int sep=0;

unsigned char outbyte, div2byte;
unsigned char keyboard_row;

unsigned char bitwise (unsigned char var, unsigned char bit, unsigned char value)
{
	if (value)
	
		return var |= 1UL << bit;
	
	else
		
		return var &= 1UL << bit;

}

void c_fiq_handler ( void )
{
	
	//  read the register	
		pGPIO.value = ptr_GPLEV0[ 0 ]; 
	//	pGPIO.value = GET32(GPLEV0);

	if (pGPIO.b.rw != 0) //read
	{

		PUT32( GPCLR0, 0x01000000); // hold the /WAIT	
			
		PUT32( GPFSEL0, 0x00249249 ); // GPIO 0 to 7 out, 8 and 9 in
	
   

		switch(pGPIO.b.ADDR)
		{
			
			//Divide by 2 interface
			case 0xC0:
				outbyte = div2byte;
				break;
			
			//read keyboard
			case 0xA9: 
				//printf("leitura A9 %d\n",keyboard_row&0x0f);
				if ((keyboard_row&0x0f) == 1) outbyte = 0b11111111; else outbyte = 0b11111111;
				break;
				
			default:
				outbyte = 0b11111111;
				break;
		}
		
				//send the "outbyte" to the bus
		PUT32( GPCLR0, 0x000000ff); //all bits as '0'	
		PUT32( GPSET0, 0x00000000 + outbyte); //set the '1' bits 
		
		PUT32( GPSET0, 0x01000000); // Pulse the GPIO 24 	
		PUT32( GPCLR0, 0x01000000); // to release the /WAIT	
	
		pGPIO.value = GET32(GPLEV0);		
		
		//just a visual debug
		put_pixel ( 200, 200, d );
		d++;
		if (d==3) d=1;

	}
	else //write
	{		
		PUT32( GPFSEL0, 0 ); // GPIO 0 to 9 in
		
		//	pGPIO.value = ptr_GPLEV0[ 0 ];  //precisa ler novamente aqui???
		
		switch( pGPIO.b.ADDR )
		{
			
			//Divide by 2 interface
			case 0xC0:
			
				div2byte = pGPIO.b.DATA >> 1;
				break;
			
			//write keyboard row
			case 0xAA:
				keyboard_row = pGPIO.b.DATA;
				
				//just a visual debug
				put_pixel ( 203, 200, d );
				d++;
				if (d==3) d=1;
				
				break;
				
			case 0xAB:
				if ((pGPIO.b.DATA>>7) & 1)
				{
					switch((pGPIO.b.DATA>>1) & 7)
					{
							case 0: keyboard_row = bitwise (keyboard_row, 0, (pGPIO.b.DATA & 1)); break;
							case 1: keyboard_row = bitwise (keyboard_row, 1, (pGPIO.b.DATA & 1)); break;
							case 2: keyboard_row = bitwise (keyboard_row, 2, (pGPIO.b.DATA & 1)); break;
							case 3: keyboard_row = bitwise (keyboard_row, 3, (pGPIO.b.DATA & 1)); break;
					}
				}
				break;
				
				//just a visual debug
				put_pixel ( 203, 200, d );
				d++;
				if (d==7) d=5;
				
				break;
		
		}
		

		
	}
	



	//clear the Pi interrupt vector
	ptr_GPEDS0[ 0 ] |= 0x8000000;  	

}


// not used right now
void c_irq_handler ( void )
{
	//clear the interrupt. 
    PUT32( ARM_TIMER_CLI, 0 );
}



//------------------------------------------------------------------------
int notmain ( void )
{
    unsigned int ra;

	initVideo();


	//-------------- configure the GPIOs
	
	PUT32( GPPUD, 0x02 ); // Enable Pull Up control 
	
	PUT32( GPFSEL0, 0 ); // GPIO 0 to 7 out, 8 and 9 in
    PUT32( GPFSEL1, 0 ); // GPIO 10 to 19 in
	PUT32( GPFSEL2, 0x1000 ); // GPIO 20 to 29 in, except 24 out
			
	//Pulse the port 24 to release the /Wait		
	PUT32( GPCLR0, 0x01000000); // port 24 = 0
	PUT32( GPSET0, 0x01000000); // port 24 = 1
	PUT32( GPCLR0, 0x01000000); // port 24 = 0
					
	ra = GET32( GPFSEL3 ); // GPIO 30 to 39
	ra &= 0xFFFFFFC0;      // GPIO 30 and 31 in
    PUT32( GPFSEL3, ra );
	

//	PUT32( GPPUDCLK0, 0xF000000 ); //GPIO 24, 25, 26,27 pull up
//	PUT32( GPAFEN0, 0xF000000 ); //GPIO 24, 25, 26, 27 falling edge
	
//	PUT32( GPPUDCLK0, 0x8000000 ); //GPIO 27 pull up
	PUT32( GPAFEN0,   0x8000000 ); //GPIO 27 falling edge
	
	ra = GET32( GPEDS0 ); 
//	ra |= 0xF000000; //GPIO 24, 25, 26 ,27
	ra |= 0x8000000; //GPIO 27
	PUT32( GPEDS0, ra ); // clear GPIO 
	
	//FIQ
	PUT32( IRQ_FIQ_CTRL, 0xB4 ); //FIQ to gpio_int[3] 
	enable_fiq();
	
	//-------------- end config GPIO	
	
	//Just print some debug information while waiting for the interrupt
	while(1)
	{
		
		//debug 
		sep=0;
		for (int i=31; i>=0; i--)
		{
			if (((pGPIO.value>>i) & 1) == 1)
				c = 4;
			else
				c = 2;
			
			sep += 3;
			
			if (i == 23   || i == 15 || i==7) sep += 3;
			
			put_pixel ( 100+ sep, 100, c );
		}
		
		sep=0;
		for (int i=31; i>=0; i--)
		{
			if (((outbyte>>i) & 1) == 1)
				c = 4;
			else
				c = 2;
			
			sep += 3;
			
			if (i == 23   || i == 15 || i==7) sep += 3;
			
			put_pixel ( 100+ sep, 104, c );
		}
		
		sep=0;
		for (int i=31; i>=0; i--)
		{
			if (((ptr_GPLEV0[0]>>i) & 1) == 1)
				c = 4;
			else
				c = 2;
			
			sep += 3;
			
			if (i == 23   || i == 15 || i==7) sep += 3;
			
			put_pixel ( 100+ sep, 108, c );
		}
		
		sep=0;
		for (int i=7; i>=0; i--)
		{
			if (((keyboard_row>>i) & 1) == 1)
				c = 4;
			else
				c = 2;
			
			sep += 3;
			
			put_pixel ( 100+ sep, 112, c );
		}

	}
	
    return(0);
}

