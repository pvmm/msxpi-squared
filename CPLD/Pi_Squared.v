//
// Pi Squared
// Copyright (c) 2019 - Victor Trucco
//
// All rights reserved
//
// Redistribution and use in source and synthezised forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
//
// Redistributions in synthesized form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution.
//
// Neither the name of the author nor the names of other contributors may
// be used to endorse or promote products derived from this software without
// specific prior written permission.
//
// THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// You are responsible for any legal issues arising from your use of this code.
//


`timescale 1ns / 100ps
`default_nettype none

module Pi_Squared 
(
		// Clocks
		input wire clock_i,

		output wire led_red,
		output wire led_yellow,
		output wire led_green,
		
		// MSX BUS
		inout reg [7:0] bus_data_io,
		input wire [15:0] bus_addr_n_i,
		input wire bus_mreq_n_i,		
		input wire bus_ioreq_n_i,
		input wire bus_wr_n_i,
		input wire bus_rd_n_i,
		input wire bus_rst_n_i,
		input wire bus_dir_i,
		input wire bus_m1_n_i,
		output wire bus_wait_n_o,		
		output wire bus_int_n_o,	
		input wire bus_rfsh_n_i,	
		input wire bus_cs1_n_i,	
		input wire bus_cs2_n_i,	
		input wire bus_sltsl_n_i,
		input wire bus_clock_i,
		output reg bus_snd_o,
		
		// Raspberry Pi BUS
		inout reg [27:0] pi_gpio
		
);


	localparam [7:0] OPL3_PORT_BASE	= 8'hC0;
	localparam [7:0] PSG_BASE			= 8'hA0;
	localparam [7:0] KEYBOARD_BASE	= 8'hA9;
	localparam [7:0] TMS9918A_BASE	= 8'h98;
	
	reg [14:0] latch_a_s;
	reg [7:0] latch_d_s;
	reg [7:0] bus_d_s;
	
	reg [7:0] keyboard_d_s;
	reg [3:0] keyboard_row_s;
	
	wire trigger_s, keyboard_s;
	wire io_wr, io_rd;
	
	assign io_wr = (bus_ioreq_n_i == 1'b0 && bus_wr_n_i == 1'b0);
	assign io_rd = (bus_ioreq_n_i == 1'b0 && bus_rd_n_i == 1'b0);
	
	reg  wait_s = 1'bZ;
	
	//we don't use the INT right now
	assign bus_int_n_o = 1'bZ; 

	// the "trigger" signal advise the Pi that an interruption occurred - ATTENTION, falling edge!
	//assign trigger_s = ( bus_ioreq_n_i == 1'b0 && (bus_addr_n_i[7:3] == OPL3_PORT_BASE[7:3]) && (bus_rd_n_i == 1'b0 || bus_wr_n_i == 1'b0) ) ? 1'b0 : 1'b1;
	//assign trigger_s = ((io_wr || io_rd) && (bus_addr_n_i[7:3] == OPL3_PORT_BASE[7:3] || bus_addr_n_i[7:2] == KEYBOARD_BASE[7:2] )) ? 1'b0 : 1'b1;
	assign trigger_s = (
							(io_wr && (bus_addr_n_i[7:2] == TMS9918A_BASE[7:2] )) //0x98 - 0x9B
					//		((io_wr || io_rd) && (bus_addr_n_i[7:3] == OPL3_PORT_BASE[7:3] )) //0 xC0 - 0xC7
					//	||	(io_wr && bus_addr_n_i[7:1] == PSG_BASE[7:1])    //write on 0xA0/A1   
					//	||	(io_wr && bus_addr_n_i[7:2] == KEYBOARD_BASE[7:2] && bus_addr_n_i[1] == 1'b1)    //write on 0xAA/AB   
					//	||	(io_rd && bus_addr_n_i[7:3] == KEYBOARD_BASE[7:3] && bus_addr_n_i[1:0] == 2'b01) //read  on 0xA9      
							) ? 1'b0 : 1'b1;

	
	always @ (negedge trigger_s, posedge pi_gpio[24])
	begin
			if (pi_gpio[24])
				begin
				
					if  (bus_rd_n_i == 1'b0) bus_d_s <= pi_gpio[7:0]; //get the outgoing byte

					wait_s <= 1'bZ; //release the /WAIT signal

				end
			else
				begin
					
					latch_d_s  <= bus_data_io;//(bus_wr_n_i == 1'b0) ? bus_data_io  : 8'bZZZZZZZZ; //tri state the Pi data bus, to get the outgoing byte
					latch_a_s <= bus_addr_n_i[14:0];
					
					pi_gpio[26] <= (bus_rd_n_i == 1'b0) ? 1'b1 : 1'b0; 		// 0 = Write,     1 = Read
					pi_gpio[25] <= (bus_ioreq_n_i == 1'b0) ? 1'b1 : 1'b0;  	// 0 = IO access, 1 = Memory Acess
					
					
					if (bus_rd_n_i == 1'b0)
					begin
						// the Pi need some time to process the request
						wait_s <= 1'b0;
					end
				end
	end
	
	
	
			
	always @(*)
	begin

		pi_gpio[22:8] <= latch_a_s;
		pi_gpio[7:0] <= (bus_rd_n_i == 1'b0) ? 8'bZZZZZZZZ : latch_d_s; //tri state the Pi data bus, to get the outgoing byte
		pi_gpio[27] <= trigger_s;  // An interrupt is on the way	
		pi_gpio[24] <= 1'bZ;
		pi_gpio[23] <= 1'bZ;
	//	bus_snd_o <= pi_gpio[23];
	
		if (io_wr && bus_addr_n_i[7:2] == KEYBOARD_BASE[7:2] && bus_addr_n_i[1:0] == 2'b10) keyboard_row_s =  bus_data_io[3:0];
		if (bus_rd_n_i == 1'b0 && trigger_s == 1'b0) bus_data_io = bus_d_s; else bus_data_io = 8'bZZZZZZZZ;
		
		if (bus_rd_n_i == 1'b0 && trigger_s == 1'b0 && bus_data_io != 8'hff)
			bus_data_io = bus_d_s; 
		//else if (keyboard_s == 1'b0) bus_data_io =  keyboard_d_s; 
		else 
			bus_data_io = 8'bZZZZZZZZ;
		
	end

	
	//send the /WAIT signal to the Z80
	assign bus_wait_n_o = wait_s;
	
	//debug LEDs
	assign led_red = ~wait_s;
	assign led_green = ~trigger_s;
	//assign led_yellow = ~keyboard_s;
	
	dac  dac 
	(
		.clk_i   (bus_clock_i),
		.res_n_i (1'b1),
		.dac_i   ( pi_gpio[23]),
		.dac_o   (bus_snd_o)
	);


endmodule